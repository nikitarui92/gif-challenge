const Telegraf = require('telegraf')
const giphy = require('giphy-api')(process.env.GIPHY_TOKEN);

const bot = new Telegraf(process.env.BOT_TOKEN)

bot.start((ctx) => {
    ctx.reply('Welcome!')
})
bot.command('random', async (ctx) => { 
    const gif = await giphy.random('jim carrey')
    console.log(gif.data)
    ctx.replyWithAnimation(gif.data.images.fixed_width.url);
})

bot.launch()
